package com.livecron.users.service.bean;

/**
 * @author Santiago Mamani
 */
public class Asus {

    private Integer price;

    private String description;

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
