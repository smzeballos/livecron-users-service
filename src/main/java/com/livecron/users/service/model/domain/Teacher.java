package com.livecron.users.service.model.domain;

import javax.persistence.*;

/**
 * @author Santiago Mamani
 */
@Entity
@Table(name = "Teacher_Table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "teacherid", referencedColumnName = "userid")
})

public class Teacher extends User {

    @Column(name = "salary", nullable = false)
    private String profession;

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
}
