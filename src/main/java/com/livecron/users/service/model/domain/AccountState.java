package com.livecron.users.service.model.domain;

/**
 * @author Santiago Mamani
 */
public enum AccountState {
    ACTIVATED,
    DEACTIVATED
}
