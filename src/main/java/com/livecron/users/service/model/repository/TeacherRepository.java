package com.livecron.users.service.model.repository;

import com.livecron.users.service.model.domain.Account;
import com.livecron.users.service.model.domain.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Santiago Mamani
 */
public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}
