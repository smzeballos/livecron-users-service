package com.livecron.users.service.controller;

import com.livecron.users.service.bean.Asus;
import com.livecron.users.service.input.AccountCreateInput;
import com.livecron.users.service.model.domain.Account;
import com.livecron.users.service.model.domain.AccountState;
import com.livecron.users.service.model.repository.AccountRepository;
import com.livecron.users.service.service.AccountCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Santiago Mamani
 */

@RequestMapping(value = "/accounts")
@RequestScope
@RestController
public class AccountController {

    // @Autowired DI por propiedad
    @Autowired
    private AccountCreateService accountCreateService;

    @Autowired
    private Asus asus;

    @Autowired
    private AccountRepository accountRepository;

    @RequestMapping(method = RequestMethod.POST)
    public Account createAccount(@RequestBody AccountCreateInput input) {
        accountCreateService.setInput(input);
        accountCreateService.execute();

        return accountCreateService.getAccount();
    }

    @RequestMapping(
            value = "/data",
            method = RequestMethod.GET
    )
    public Account findAccountByEmailAndState(@RequestParam("email") String email,
                                              @RequestParam("state") AccountState state) {
        return accountRepository.findByEmailAndState(email, state).orElse(null);
    }

    @RequestMapping(
            value = "/bean",
            method = RequestMethod.GET
    )
    public Asus findAsus() {
        System.out.println("Price: " + asus.getPrice());//Ctrl +D
        System.out.println("Description: " + asus.getDescription());
        asus.setPrice(12000);
        return asus;
    }

    @RequestMapping(
            value = "/beanSecond",
            method = RequestMethod.GET
    )
    public Asus findAsusSecond() {
        System.out.println("Price: " + asus.getPrice());//Ctrl +D
        System.out.println("Description: " + asus.getDescription());
        return asus;
    }
}
