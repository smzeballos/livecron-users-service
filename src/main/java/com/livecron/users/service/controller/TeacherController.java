package com.livecron.users.service.controller;

import com.livecron.users.service.input.TeacherCreateInput;
import com.livecron.users.service.model.domain.Teacher;
import com.livecron.users.service.service.TeacherCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Santiago Mamani
 */
@RequestMapping(value = "/teachers")
@RequestScope
@RestController
public class TeacherController {

    @Autowired
    private TeacherCreateService teacherCreateService;

    @RequestMapping(method = RequestMethod.POST)
    public Teacher createTeacher(@RequestBody TeacherCreateInput input) {
        teacherCreateService.setInput(input);
        teacherCreateService.execute();

        return teacherCreateService.getTeacher();
    }
}
