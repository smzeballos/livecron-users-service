package com.livecron.users.service;

import com.livecron.users.service.bean.Asus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author Santiago Mamani
 */
@Configuration
public class Config {

    @Bean()
    @Scope("prototype")
    public Asus asus() {
        Asus asus = new Asus();
        asus.setDescription("I am an Bean, ASUS");
        return asus;
    }
}
